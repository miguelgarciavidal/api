//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var requestJson = require('request-json');
var urlMLabClientes = 'https://api.mlab.com/api/1/databases/mgarcia/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var urlMLabClients = 'https://api.mlab.com/api/1/databases/mgarcia/collections/Client?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';

var clienteMlab = requestJson.createClient(urlMLabClientes);
var clientProyect = requestJson.createClient(urlMLabClients);

var movimientos = require('./movs2.json');
var bodyparser = require('body-parser');

var googleApiKey = 'AIzaSyDvVBHrLMkURDE679rQeuzNO7-YAJPoakI';

var urlMLabRaiz = 'https://api.mlab.com/api/1/databases/mgarcia/collections/';
var apiKey = 'apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var clientMlabRaiz;

app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Header","Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ('/',function(req,res) {
  //res.send('Hello World NodeJS');
  res.sendFile(path.join(__dirname,'index.html'));
})
app.post('/', function(req,res) {
  res.send('Petición Post solicitada');
})
app.put('/',function(req,res) {
  res.send('Put request recived');
})
app.delete('/',function(req,res) {
  res.send('Delete request recived');
})
//GET with Data Client
app.get ('/Clients/:idClient',function (req,res) {
  res.send('Heir is the client number: ' + req.params.idClient);
})
//List all Clients
app.get ('/Movimientos',function (req,res) {
  //sendfile with a URL
  res.sendfile('movs.json');
})
//List all Clients
app.get ('/v2/Movimientos',function (req,res) {
  res.json(movimientos);
})
//List Client with the id requested
app.get ('/v2/Movimientos/:id',function (req,res) {
  console.log(req.params.id);
  res.send(movimientos[req.params.id-1]);
})
//List Client with the id requested
app.get ('/v2/Movimientosq',function (req,res) {
  console.log(req.query);
  res.send('Query recived: ' + req.query);
})

app.post('/v2/Movimientos/',function (req, res) {
  var nuevo = req.body;
  nuevo.id = movimientos.length + 1;
  movimientos.push(nuevo);
  res.send('Movimientos dado de alta: ' + nuevo.id);
})
app.get ('/v1/Clientes',function (req, res) {
  clienteMlab.get ('',function(err, resM, body) {
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

app.post('/v1/Clientes',function(req, res) {
  clienteMlab.post('',req.body, function(err, resM, body) {
    res.send(body);
  })
})

app.get('/v1/Client/:idclient', function(req,res) {
    var query = '&q={"idclient":'+req.params.idclient+'}'
    var filter = '&f={"first_name":1,' +
    '"last_name":1,'+
    '"email":1,' +
    '"user_name":1, ' +
    '"accounts.account":1,' +
    '"accounts.card":1,' +
    '"_id":0},'
    var uri = urlMLabClients + query + filter;
    clientProyect.get(uri,function(err, resM, body) {
      if (err) {
        console.error('Error, petición vacia');
        console.error(body);
      } else {
        res.send(body);
      }

    })
})

app.post('/v2/login',function(req, res) {
  var username = req.body.username;
  var pass = req.body.password;
  var jsonquery = '{"username":"' + username + '", "password":"' + pass + '"}';
  var query = urlMLabRaiz + "Usuarios?" + apiKey +'&q=' + jsonquery;
  //console.log(query);
  clientMlabRaiz = requestJson.createClient( query );
  clientMlabRaiz.get('',function(err, resM, body) {
    if (!err && body.length == 1) {//Login OK
      res.status(200).send('Usuario logueado');
    } else {
      res.status(404).send('Usuario NO logueado');
    }
  })
})

app.get('/v1/Client/:idclient/Movments/:account', function(req,res) {
    var query = '&q={"idclient":'+req.params.idclient+
      ',"accounts.account":"' + req.params.account + '"}'
    var filter = '&f={' +
      '"_id":0,' +
      '"accounts.$":1}'
    var uri = urlMLabClients + query + filter;
    clientProyect.get(uri,function(err, resM, body) {
      if (err) {
        console.error('Error, petición vacia');
        console.error(body);
      } else {
        res.send(body);
      }

    })
})
